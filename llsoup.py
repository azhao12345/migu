from __future__ import division
from bs4 import BeautifulSoup
import requests
import json


text = requests.get('http://love-live.wikia.com/wiki/Category:Discography').text
soup = BeautifulSoup(text)
pages = soup.find('div', {'id': 'mw-pages'})
links = [a['href'] for a in pages.findAll('a')]
print links
print len(links)

for link in links:
    print link
    text = requests.get('http://love-live.wikia.com' + link).text
    soup = BeautifulSoup(text)
    kanji = soup.find('div', {'class': 'tabbertab', 'title': 'Kanji'})

    if kanji == None:
        continue

    output = open('out/' + link.split('/')[2], 'w')
    output.write(kanji.text.encode('utf-8'))
    output.close()

'use strict';

var kuromoji = require('kuromoji');
var fs = require('fs');

kuromoji.builder({}).build((err, tokenizer) => {
    var words = { };
    var text = fs.readFileSync('sample.txt').toString('utf-8');
    //console.log(text);
    text.split('\n').forEach((line) => {
        var result = tokenizer.tokenize(line);
        result.forEach((word) => {
            if (words[word.basic_form] == undefined) {
                words[word.basic_form] = [];
            }
            word.line = line;
            words[word.basic_form].push(word)
        });
    });

    var wordsArray = Array.from(Object.keys(words), (key) => [
        words[key][0].basic_form,
        new Set(words[key].map((instance) => instance.reading)),
        new Set(words[key].map((instance) => instance.surface_form)),
        words[key].length,
        new Set(words[key].map((instance) => instance.line)),
        ]).filter(word => word[3] <= 18);
    wordsArray.sort((a, b) => b[3] - a[3]);
    wordsArray.forEach((word) => {
        console.log(word[0] + ';' + Array.from(word[1]).toString() + ';' + Array.from(word[2]).toString() + ';' + slorg(word[4]))
    });
});

function slorg(s) {
    return Array.from(s).toString().replace(/,/g, '<br/>');
}
